# Vomter

This application was created as a part of a university project. The main function is to calculate volume of a certain biological structure given by its binary image. It was created using Matlab R2017A and GUIDE tool.

Result example:
![app screenshot](./images/screen-1.png)

## Credits
* Image was taken from https://medpix.nlm.nih.gov/case?id=b489bf24-efab-42b5-9a34-dc258aca9872
* Some functions were written earlier in my thesis. See: Kubacki J.: _Algorytm procesu przebudowy kości gąbczastej techniką Monte Carlo_, Wrocław 2019

## LICENSE
You can use this code without any restriction. I don't take resposibility for the results.