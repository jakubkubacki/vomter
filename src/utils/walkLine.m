function [r, xn, yn] = walkLine(img, x, y, a, direction)
    foundBorder = false;
    r = 0;
    xn = x;
    yn = y;
    alpha = atan(a);
    while ~foundBorder
        xnext = xn + direction * r * cos(alpha);
        ynext = yn + direction * r * sin(alpha);
        
        if xor(isInImage(img, [xnext, ynext]), isInImage(img, [xn, yn]))
            foundBorder = 1;
        end

        r = r + 0.1;
        xn = xnext;
        yn = ynext;
    end
end

