function [V, Rs, Pss] = getVolume(img, a, b, fromX, toX)
    
    fromY = fromX * a + b;
    toY = toX * a + b;
    
    lineLength = floor(getDistance([fromX, fromY], [toX, toY]));
    
    alpha = atan(a);
    
    Rs = [];
    Pss = [];
    
    xn = fromX;
    while xn < toX
        yn = a * xn + b;
        xn = xn + 0.1;
        if isInImage(img,[xn, yn])
            [ap] = perpendicularLine(a, b, xn);

            [R, Ps] = getCircle(img, xn, yn, ap);
            Rs = [Rs; R];
            Pss = [Pss; Ps];
        end
    end
   
    V = 0;
    dH = lineLength / length(Rs);
    for i = 2:length(Rs)
        dR = 0.5 * (Rs(i) - Rs(i - 1));
        if dR ~= 0
            H = (Rs(i) * dH) ./ (2 * dR);

            R1 = max([Rs(i), Rs(i - 1)]);
            R2 = min([Rs(i), Rs(i - 1)]);
            dV = 1/3 * pi * (R1 * R1 * H - R2 * R2 * (H - dH));
        else
            dV = pi * Rs(i) * Rs(i) * dH;
        end

        V = V + dV;
    end
    
end

