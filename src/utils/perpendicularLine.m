function [a2, b2] = perpendicularLine(a1, b1, x)
    a2 = -1 ./ a1;
    b2 = x .* (a1 + 1 ./ a1) + b1;
end

