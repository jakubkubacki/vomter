function [R, Ps] = getCircle(img, x, y, a)
    
    [r1, xn1, yn1] = walkLine(img, x, y, a, 1);
    [r2, xn2, yn2] = walkLine(img, x, y, a, -1);
    
    R = mean([r1, r2]);
    Ps = [ ...
        xn1, yn1;
        xn2, yn2
    ];
    
end

