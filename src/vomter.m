function varargout = vomter(varargin)
% VOMTER MATLAB code for vomter.fig
%      VOMTER, by itself, creates a new VOMTER or raises the existing
%      singleton*.
%
%      H = VOMTER returns the handle to a new VOMTER or the handle to
%      the existing singleton*.
%
%      VOMTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VOMTER.M with the given input arguments.
%
%      VOMTER('Property','Value',...) creates a new VOMTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before vomter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to vomter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help vomter

% Last Modified by GUIDE v2.5 03-Jun-2019 14:31:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @vomter_OpeningFcn, ...
                   'gui_OutputFcn',  @vomter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before vomter is made visible.
function vomter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to vomter (see VARARGIN)

% Choose default command line output for vomter
handles.output = hObject;
handles.clicked = 0;


addpath('utils');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes vomter wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = vomter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btnOpen.
function btnOpen_Callback(hObject, eventdata, handles)
    [file,path] = uigetfile(['*.png'], 'Open image');
    fullPath = [path, file];
    
    image = imread(fullPath);
    handles.img = im2bw(image);
    
    axes(handles.imgAxes);
    gcf = imshow(handles.img);
    
    set(gcf, 'ButtonDownFcn', @imgAxes_ButtonDownFcn);
    
    guidata(hObject, handles);
    
    

% --- Executes on button press in btnSelect.
function btnSelect_Callback(hObject, eventdata, handles)
    handles.clicked = 1;
    guidata(hObject, handles);


% --- Executes on mouse press over axes background.
function imgAxes_ButtonDownFcn(hObject, eventdata)
    handles = guidata(hObject);
    if handles.clicked == 1
        handles.point1 = eventdata.IntersectionPoint;
        handles.clicked = 2;
        
        
    elseif handles.clicked == 2
        handles.point2 = eventdata.IntersectionPoint;
        handles.clicked = 0;

        hold(handles.imgAxes, 'on');
        imshow(handles.img)
        plot( ...
            [handles.point1(1), handles.point2(1)], ...
            [handles.point1(2), handles.point2(2)], ...
            'r');
        hold(handles.imgAxes, 'off');
        
        
        calculateVolumn(hObject, handles);
    end

    guidata(hObject, handles);



function calculateVolumn(hObject, handles)
    x1 = handles.point1(1);
    x2 = handles.point2(1);

    y1 = handles.point1(2);
    y2 = handles.point2(2);

    handles.a = (y2 - y1) ./ (x2 - x1);
    handles.b = (x1 * y2 - x2 * y1) ./ (x1 - x2);

    fromX = min([x1 x2]);
    toX = max([x1 x2]);

    [V, Rs, Pss] = getVolume(handles.img, handles.a, handles.b, fromX, toX);

    hold(handles.imgAxes, 'on');
    imshow(handles.img)
    plot( ...
        [handles.point1(1), handles.point2(1)], ...
        [handles.point1(2), handles.point2(2)], ...
        'r');
    plot(Pss(:, 1), Pss(:, 2), 'r*');
    hold(handles.imgAxes, 'off');

    disp(['Volume = ', num2str(V), ' px3']);
    handles.textResult.String = ['Volume = ', num2str(V), ' px3'];

    guidata(hObject, handles);
